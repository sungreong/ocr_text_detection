## 전반적인 간단한 코드 설명

1. annotation tool을 활용하여 image를 annotation 후 raw code에 맞게 변환 및 학습시키는 코드 
2. 추가) 현재 SOTA를 찍은 CRAFT 코드를 통해서 평가만 가능


## paper  <br>
https://arxiv.org/abs/1903.12473

## raw code  <br>
https://github.com/liuheng92/tensorflow_PSENet

## annotation tool  <br>
https://github.com/tzutalin/labelImg#labelimg

## 주요 라이브러리 버전  <br>

* torch == 1.1.0
* tensorflow == 1.13.1
* cv2 == 4.1.0


## 사용법

## 1. CLOVA API를 활용해서 Annotation 후 PSENet에 맞게 변환하기
    
1. CLOVA에서 ANNOTATION 된 것을 html 전체 창에서 저장한다. [URL](https://demo.ocr.clova.ai/)
2. 원래 이미지 ->  **img** 폴더 , 생성 된 것 -> **gt** 폴더에 넣기 
3. 변환 작업은 hahaha.py를 load하는 **Clova_API.ipynb**  를 이용하면 됨.
4. 출력물 <br>
        1 .annotation된 이미지(확인 하기 위해)  <br>
        2. (x,y) 4쌍의 좌표  <br>
        3. xml  <br>
5. 원본 이미지와 4)에서 출력된 xml을 활용해 부족한 부분 annotation
6. **newdata_append** 폴더에 **img** , **xml** 넣기
7. **labeling.ipynb**을 활용해서 Train data 위치에 만든 데이터 넣어주기

## 2. 학습된 PSENet를 활용해서 Annotation 빠르게 하기

1. annotation하고 싶은 이미지를 Eval.py 실행
2. **학습된 PSENet 이용하여 annotation 미리하기.ipynb**에 돌아가는 코드 있음.
3. psenet2xml 함수를 실행하면 됨 <br>
    * predict_dir에 1)에 Eval된 이미지의 경로를 지정해주면 해당 폴더에 result 폴더 생성됨. <br>
    출력물 : **xml**
4. 3)에서 나온 출력물과 원본 이미지를 바탕으로 anootation tool 사용하여 수정 및 보완하기

## 3.  PSENet train 하는 법 


<br>

* 학습 중에 데이터 넣어주기 <br>
    **사용법  1.7**을 하게 되면 학습 중에도 데이터 추가로 넣어주는 것이 가능함.

```
python train.py --gpu_list=1 --input_size=512 --batch_size_per_gpu=8 --checkpoint_path=./../pretrained_model/model.ckpt/model.ckpt-449461  --training_data_path=./newdata
```

<br>

## 4. PSENet eval 하는 법

<br>

**PSENet_tf/Eval 및 시각화.ipynb**  참고

```
python eval.py --test_data_path=./noleak --gpu_list=0 --checkpoint_path=./../pretrained_model/Trained/0530/0530_2/0605/0605_2/0606/model.ckpt-2883014/0704/model.ckpt-7958085 --output_dir=./result0707_no --distance 0.15 --max_side_len 3000 --seg_map_thresh 0.9 --min_area_thresh 2 --nms_thresh 0.0 
```

## 5. CRAFT 사용법

<br>

* CLOVA의 Pytorch CRAFT를 통한 비교 [URL](https://github.com/clovaai/CRAFT-pytorch) <br>

**CRAFT-pytorch/EVAL.ipynb** 참고

```
python test.py --test_folder=./../tensorflow_PSENet/noleak --output_folder=./result0707_no/ --trained_model=./Pretrained/craft_mlt_25k.pth --text_threshold 0.01 --low_text 0.2 --link_threshold 0.1 --canvas_size 1800 
```


