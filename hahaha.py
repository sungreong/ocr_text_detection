import cv2 
import os , re
import numpy as np , pandas as pd
from xml.etree.ElementTree import Element, dump, SubElement, ElementTree
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
def resizeShape(img, img_gt) : 
    ### 이미지 조정? 
    ### 크든 작든 한개로 고정해주기
    if img.shape[0] > img.shape[1] : 
        offset = (img.shape[1] * img_gt.shape[0] // img.shape[0] )
        first = (img_gt.shape[1] - offset) // 2 
        img_gt = img_gt[:, first : (img_gt.shape[1] + offset) // 2 ]
        #img_gt = img_gt[:, (img_gt.shape[1] - offset) // 2:(img_gt.shape[1] + offset) // 2 ]
    else : 
        offset = (img.shape[0] * img_gt.shape[1] // img.shape[1] )
        img_gt = img_gt[(img_gt.shape[0] - offset) // 2:(img_gt.shape[0] + offset) // 2 ,:]
    img_gt = cv2.resize(img_gt, (img.shape[1],img.shape[0]))
    return img_gt

def findPoints(img, img_gt ) : 
    # green color 로 일단 색깔 입히기?
    lower = (0, 100, 0)
    upper = (255, 255, 255)
    h , w = img_gt.shape[0] , img_gt.shape[1]
    ## 여기서 에러 발생 3D -> 2D로 되버림 
    img_mask = cv2.inRange(img_gt, lower, upper )
    i = img.copy()
    ## 여기서 Point 찾는데 Hireachy 하게 들어가는 것을 어떻게 처리할지? 
    #### 1. 겹치는 것의 distance를 계산을 해서 너무 근처에 있으면 저게를 할지?
    #### ### 문제 발생 2  여기서 안됨!
    cnts , _  = cv2.findContours(img_mask,  cv2.RETR_CCOMP ,cv2.CHAIN_APPROX_SIMPLE ) # cv2.RETR_EXTERNAL
    point3 = []
    point2 = []
    point = []
    for c in cnts : 
        rect = cv2.minAreaRect(c)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        check = np.array(box)[[0 , 2]]
        check = np.array([
            [check[0,0]/w , check[0,1]/h],
            [check[1,0]/w , check[1,1]/h]]
        )
        endpoint = np.array([max(check[:,0]) , max(check[:,1])])
        startpoint = np.array([min(check[:,0]) , min(check[:,1])])
        distance= np.linalg.norm(startpoint- endpoint )
        #distance= np.linalg.norm(check[0,:] - check[1,:])
        if distance > 0.15 :
            continue
        dd = ",".join(list(map(lambda x : str(x),box.flatten().tolist())))+",###"
        x = box[:,0]
        y = box[:,1]
        xx , yy = np.mean(x) , np.mean(y)
        point2.append([xx,yy])
        point3.append(dd)
        point.append(box)
    
    
    #slice_ = pd.DataFrame(point2).drop_duplicates(keep= "first").index.values.tolist()
    pointt = np.array(point2)
    pointt[:,0] = pointt[:,0]/w
    pointt[:,1] = pointt[:,1]/h
    total = pd.DataFrame(pointt)
    metric = "euclidean"
    dist_ex1 = cdist( total , total, metric=metric )
    slice_ = pd.DataFrame(dist_ex1 < 0.01).drop_duplicates().index.tolist()
    box2 = np.array(point)[slice_]
    point = np.array(point3)[slice_]
    for box in box2 :
        im = cv2.drawContours(i,[box],0,(255,0,0),1)

    return point, im

def Xml(filename, path, width_, height_, depth_, point) :
    annotation = Element("annotation")
    l = ["folder", 'filename', "path", 'source', 'size', 'segmented']
    for i in l : 
        child = SubElement(annotation, i)
        if i == "folder" : 
            child.text = "data"
        elif i == "filename" : 
            child.text = filename
        elif i == "path" : 
            child.text = path
        elif i == "source" : 
            database = SubElement(child, "database")
            database.text = "Unknown"
        elif i == "size" :
            w = SubElement(child,"width")
            h = SubElement(child,"height")
            d = SubElement(child,"depth")
            w.text = str(width_)
            h.text = str(height_)
            d.text = str(depth_)
        elif i == "segmented" :
            child.text = "0"

    for p in point : 
        p = p.split(",")
        xmin_ = min(p[0],p[2],p[4],p[6])
        xmax_ = max(p[0],p[2],p[4],p[6])
        ymin_ = min(p[1],p[3],p[5],p[7])
        ymax_ = max(p[1],p[3],p[5],p[7])
        child = SubElement(annotation, "object") 
        name = SubElement(child, "name")
        pose = SubElement(child, "pose")
        truncated = SubElement(child, "truncated")
        difficult = SubElement(child, "difficult")
        bndbox = SubElement(child, "bndbox")
        xmin = SubElement(bndbox, "xmin")
        ymin = SubElement(bndbox, "ymin")
        xmax = SubElement(bndbox, "xmax")
        ymax = SubElement(bndbox, "ymax")

        name.text = p[-1]
        pose.text = "Unspecified"
        truncated.text = "0"
        difficult.text = "0"
        xmin.text = xmin_
        ymin.text = ymin_
        xmax.text = xmax_
        ymax.text = ymax_
    
   # dump(annotation)
    ElementTree(annotation).write(args.dir+ "/results/" + filename[:-3]+"xml")

if __name__ == "__main__" : 
    ## argument
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-dir",
                    type = str,
                    help = "Directory that contain both image and ground truth")
    args = parser.parse_args()
    
    if os.path.exists(args.dir+"/results") is False : 
        os.mkdir(args.dir+"/results")
    
    img_list = os.listdir(args.dir +"/img/")
    gt_list  = os.listdir(args.dir +"/gt/")
    #img2 = [re.split(".jpg|.png.|jpeg",i )[0] for i in img_list]
    
    for img in img_list : 
        try :
            checkpoint = re.split(".jpg|.png|.jpeg",img)[0]
            image = cv2.imread(args.dir+"/img/" + img)
            gt = cv2.imread(args.dir+"/gt/"+checkpoint + ".png")
            ## img 불러서 사이즈 조정하기 
            gt = resizeShape(image, gt)
            
            point, im = findPoints(image, gt)
            ## 점과 이미지 업데이트 된 것 불러들이기  
            print(args.dir+"/results/" + img[:-3] + "txt")
            with open(args.dir+"/results/" + img[:-3] + "txt", "w") as f : 
                for p in point : 
                    f.write(p+"\n")
            print("%s : Done" % img[:-4])
            cv2.imwrite(args.dir+"results/"+img[:-3]+"jpg", im)
            # make Xml 
            Xml(img, args.dir+"/img/" + img, image.shape[1], image.shape[0], image.shape[2], point)
        except Exception as e :
            print(e)
            continue
    print("Finish!!")
    
